import pandas as pd
import torch

labels = ['Iris-setosa', 'Iris-versicolor', 'Iris-virginica']
labelsToIdx = {
    'Iris-setosa': 0,
    'Iris-versicolor': 1,
    'Iris-virginica': 2
}

def getNpyFromCsv(csv_file, replace_dict = None):
    df = pd.read_csv(csv_file, index_col = 0, header=0)
    if replace_dict:
        df = df.replace(replace_dict)
    data = df.to_numpy()
    return data

def getAccuracy(preds, true_labels, groupByClass = False):
    if groupByClass:
        correctByClass = [0]*preds.shape[1]
    else:
        correct = 0
    predIndices = torch.max(preds, 1).indices
    for pred, true in zip(predIndices, true_labels):
        if pred == true:
            if groupByClass:
                correctByClass[true] += 1
            else:
                correct += 1
    if groupByClass:
        return correctByClass
    else:
        return correct

params = {
    'batch-size': 16,
    'epochs': 75,
    'lr': 10e-3,
    'weight-decay': 5*10e-4,
    'momentum': 0.9
}