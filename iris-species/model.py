import torch.nn as nn
import torch.nn.functional as func

class Model(nn.Module):
    def __init__(self):
        super(Model, self).__init__()
        self.classifier = nn.Sequential(
            nn.Linear(4, 10),
            nn.ReLU(inplace=True),
            nn.Linear(10, 20),
            nn.ReLU(inplace=True),
            nn.Linear(20, 10),
            nn.ReLU(inplace=True),
            nn.Linear(10, 3),
            nn.Softmax(dim = 1)
        )
    
    def forward(self, x):
        return self.classifier(x)