import torch
from torch.utils.data import Dataset

class IrisDataset(Dataset):
    def __init__(self, X, y):
        self.X = X
        self.y = y

    def __len__(self):
        return len(self.X)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        x = self.X[idx]
        label = int(self.y[idx])

        sample = {'x': x, 'y': label}

        return sample