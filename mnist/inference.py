import argparse
import torch
from torch.utils.data import DataLoader
from torchsummary import summary

from model import MNISTModel
from dataloader import MNISTTestDataset

def loadModel(modelFile):
    model = MNISTModel()
    model.load_state_dict(torch.load(modelFile))
    model = model.float()
    model.eval()
    return model

def loadInput(inputFile):
    dataset = MNISTTestDataset(inputFile)
    dataloader = DataLoader(dataset, batch_size=100, shuffle=False)
    return dataset, dataloader

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--inputFile", help="Input file to be used for inference", default="./data/test.csv")
    parser.add_argument("--modelFile", help="Model file", default="./models/MNISTCNNmodel.pth")
    parser.add_argument("--outputFile", help="Output file for predictions", default="./data/predicted.csv")

    args = parser.parse_args()
    inputFile = args.inputFile
    modelFile = args.modelFile
    outputFile = args.outputFile

    dataset, loader = loadInput(inputFile)
    print("Loaded dataset. Samples: {}, batches: {}".format(len(dataset), len(loader)))

    model = loadModel(modelFile)
    print("Loaded model:")
    summary(model, input_size=(1, 28, 28))

    # Lets start inference:
    fp = open(outputFile, 'w+')
    fp.write("ImageId,Label\n")
    idx = 1
    for _, batch in enumerate(loader):
        ip = batch['x']
        op = model.forward(ip.float())
        pred_label = op.data.max(1, keepdim=True)[1]
        for label in pred_label:
            fp.write("{},{}\n".format(idx, label.numpy()[0]))
            print("{} prediction(s) written.".format(idx), end="\r")
            idx += 1
    print("All predictions written to {}".format(outputFile))
    fp.close()

if __name__ == "__main__":
    main()