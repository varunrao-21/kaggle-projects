import torch
import torch.nn as nn

def getAccuracy(preds, true_labels, groupByClass = False):
    if groupByClass:
        correctByClass = [0]*preds.shape[1]
    else:
        correct = 0
    predIndices = torch.max(preds, 1).indices
    for pred, true in zip(predIndices, true_labels):
        if pred == true:
            if groupByClass:
                correctByClass[true] += 1
            else:
                correct += 1
    if groupByClass:
        return correctByClass
    else:
        return correct

def weightInit(m):
    if type(m) == nn.Linear:
        nn.init.xavier_uniform_(m.weight)
        m.bias.data.fill_(0.01)