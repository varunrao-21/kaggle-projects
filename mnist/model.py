import torch
import torch.nn as nn
import torch.nn.functional as F
import utils

class MNISTModel(nn.Module):
    def __init__(self):
        super(MNISTModel, self).__init__()
        self.feature_extractor = nn.Sequential(
            nn.Conv2d(1, 10, kernel_size=3),
            nn.MaxPool2d(2),
            nn.ReLU(),
            nn.Conv2d(10, 20, kernel_size = 3),
            nn.MaxPool2d(2),
            nn.ReLU(),
            nn.Conv2d(20, 30, kernel_size = 3),
            nn.Dropout2d(),
            nn.ReLU()
        )
        self.classifier = nn.Sequential(
            nn.Linear(270, 800),
            nn.ReLU(),
            nn.Dropout(),
            nn.Linear(800, 600),
            nn.ReLU(),
            nn.Dropout(),
            nn.Linear(600, 10)
        )

    def forward(self, x):
        x = self.feature_extractor(x)
        
        x = x.view(-1, 270)
        
        x = self.classifier(x)

        return F.log_softmax(x, dim=1)

class MNISTDNNModel(nn.Module):
    def __init__(self):
        super(MNISTDNNModel, self).__init__()
        self.fc1 = nn.Linear(784, 800)
        self.fc2 = nn.Linear(800, 800)
        self.fc3 = nn.Linear(800, 10)

    def forward(self, x):
        x = F.sigmoid(self.fc1(x))
        x = F.sigmoid(self.fc2(x))
        x = self.fc3(x)
        return F.log_softmax(x, dim = 1)