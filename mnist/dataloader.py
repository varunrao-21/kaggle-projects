from torch.utils.data import Dataset
import numpy as np
import torch

class MNISTDataset(Dataset):
    def __init__(self, csv_file, as_img = True, transforms = None):
        data = np.loadtxt(csv_file, delimiter = ',', skiprows=1)
        
        self.x = data[:,1:]
        self.y = data[:,0]
        
        if as_img:
            n, _ = self.x.shape
            self.x = np.reshape(self.x, (n, 28, 28, 1))

        self.y = self.y.astype(int)

        self.transforms = transforms
    
    def __len__(self):
        return len(self.x)
    
    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()
        
        img = self.x[idx]
        label = self.y[idx]

        if self.transforms:
            img = self.transforms(img)

        sample = {'x': img, 'y': label}

        return sample

class MNISTTestDataset(Dataset):
    def __init__(self, csv_file, as_img = True, transforms = None):
        data = np.loadtxt(csv_file, delimiter = ',', skiprows=1)
        
        self.x = data
        
        if as_img:
            n, _ = self.x.shape
            self.x = np.reshape(self.x, (n, 1, 28, 28))

        self.transforms = transforms
    
    def __len__(self):
        return len(self.x)
    
    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()
        
        img = self.x[idx]

        if self.transforms:
            img = self.transforms(img)

        sample = {'x': img}

        return sample