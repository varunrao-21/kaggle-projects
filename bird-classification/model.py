import torch.nn as nn
import torch.nn.functional as func

class ClassifierModel(nn.Module):
    def __init__(self):
        super(ClassifierModel, self).__init__()
        self.conv1_1 = nn.Conv2d(3, 64, 3, padding=1)
        self.pool1 = nn.MaxPool2d(2, 2)
        
        self.conv2_1 = nn.Conv2d(64, 128, 3, padding=1)
        self.pool2 = nn.MaxPool2d(2, 2)
        
        self.conv3_1 = nn.Conv2d(128, 256, 3, padding=1)
        self.conv3_2 = nn.Conv2d(256, 256, 3, padding=1)
        self.pool3 = nn.MaxPool2d(2, 2)
        
        self.conv4_1 = nn.Conv2d(256, 512, 3, padding=1)
        self.conv4_2 = nn.Conv2d(512, 512, 3, padding=1)
        self.pool4 = nn.MaxPool2d(2, 2)
        
        self.conv5_1 = nn.Conv2d(512, 512, 3, padding=1)
        self.conv5_2 = nn.Conv2d(512, 512, 3, padding=1)
        self.pool5 = nn.MaxPool2d(2, 2)

        self.fc1 = nn.Linear(512*7*7, 4096)
        self.drop1 = nn.Dropout(p = 0.5)
        self.fc2 = nn.Linear(4096, 4096)
        self.drop2 = nn.Dropout(p = 0.5)
        self.predict = nn.Linear(4096, 150)
    
    def forward(self, x):
        x = self.pool1(func.relu(self.conv1_1(x)))
        x = self.pool2(func.relu(self.conv2_1(x)))
        x = self.pool3(func.relu(self.conv3_2(self.conv3_1(x))))
        x = self.pool4(func.relu(self.conv4_2(self.conv4_1(x))))
        x = self.pool5(func.relu(self.conv5_2(self.conv5_1(x))))

        x = x.view(-1, 512*7*7)
        x = func.relu(self.drop1(self.fc1(x)))
        x = func.relu(self.drop2(self.fc2(x)))
        final_op = self.predict(x)
        predictions = func.softmax(final_op, dim=1)
        return predictions