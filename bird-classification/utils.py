import torch
import matplotlib.pyplot as plt
import numpy as np

def getLabelIdx(labelsFile):
    label_to_num_idx = dict()
    with open(labelsFile, 'r') as f:
        line = f.readline()
        keyNames = line.split(",")
        for keyName in keyNames:
            splitted = keyName.split(":")
            if len(splitted) == 2:
                num, name = splitted
                label_to_num_idx[name] = int(num)
    return label_to_num_idx

def getAccuracy(preds, true_labels, groupByClass = False):
    if groupByClass:
        correctByClass = [0]*preds.shape[1]
    else:
        correct = 0
    predIndices = torch.max(preds, 1).indices
    for pred, true in zip(predIndices, true_labels):
        if pred == true:
            if groupByClass:
                correctByClass[true] += 1
            else:
                correct += 1
    if groupByClass:
        return correctByClass
    else:
        return correct

def show(img):
    npimg = img.numpy()
    plt.imshow(np.transpose(npimg, (1,2,0)))

hyperparameters = {
    'lr': 10e-2,
    'epochs': 3,
    'train-batch-size': 4,
    'momentum': 0.9,
    'weight-decay': 5*10e-4
}