import cv2
import torch
from torch.utils.data import Dataset
from glob import glob

class BirdDataset(Dataset):
    def __init__(self, root_dir, label_idx_dict, transform = None):
        if root_dir[-1] == "/":
            folders = glob(root_dir+"*/*")
        else:
            folders = glob(root_dir+"/*/*")
        self.images = []
        self.labels = []
        for folder in folders:
            label = folder.split("/")[-2]
            image = cv2.imread(folder)
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            self.images.append(image)
            self.labels.append(label_idx_dict[label])
        self.transform = transform
    
    def __len__(self):
        return len(self.images)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        image = self.images[idx]
        label = self.labels[idx]

        if self.transform:
            image = self.transform(image)
        
        sample = {'image': image, 'label': label}

        return sample